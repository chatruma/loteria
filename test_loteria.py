import pytest
from lotery import Decimo, Sorteo


def test_decimo_numero():
    dec = Decimo(100)
    assert dec.num == 100


def test_decimo_aleatorio():
    dec = Decimo()
    assert 0 <= dec.num <= 99999


def test_decimo_num_lista():
    dec = Decimo(12345)
    assert dec.num_list == [1, 2, 3, 4, 5]


def test_decimo_equals():
    decimo = Decimo(123)
    assert decimo.equals(123)
    assert not decimo.equals(4444)


@pytest.mark.parametrize(
    "numero,n,expected",
    [
        (1234, 1, False),
        (46755, 1, True),
        (32645, 2, True),
    ]
)
def test_decimo_lastn(numero, n, expected):
    decimo = Decimo(12345)
    assert decimo.last_n(numero, n) == expected


@pytest.mark.parametrize(
    "numero,expected",
    [
        (12333, True),
        (12377, True),
        (12300, True),
        (12399, True),
        (12400, False),
        (12299, False),
    ]
)
def test_decimo_hundreds(numero, expected):
    decimo = Decimo(12345)
    assert decimo.hundreds(numero) == expected


@pytest.mark.parametrize(
    "numero,expected",
    [
        (12346, True),
        (12344, True),
        (12349, False),
    ]
)
def test_decimo_aprox(numero, expected):
    decimo = Decimo(12345)
    assert decimo.aprox(numero) == expected


def test_sorteo_with_numberfs():
    s = Sorteo([1, 2, 3, 4])
    assert s.numbers == [1, 2, 3, 4]


def test_sorteo_wo_numbers():
    s = Sorteo()
    assert len(s.numbers) == 1807


def sorteo():
    nums = [
        12345,
        23456,
        34567,
        45678,
        56789,
        67890,
        78901,
        89012,
        90123,
        1234,
        11234,
        22345,
        33456,
        11111,
        77775, # Pedrea y reintegro
        66645, # Pedrea y terminación con primero
        55556, # Pedrea y terminación con segundo
        44467, # Pedrea y terminación con cuarto
    ]
    return Sorteo(nums)


@pytest.mark.parametrize(
    "num,prize",
    [
        (12345, 400000),
        (23456, 125000),
        (34567, 50000),
        (45678, 20000),
        (56789, 20000),
        (67890, 6000),
        (78901, 6000),
        (89012, 6000),
        (90123, 6000),
        (1234, 6000),
        (11234, 6000),
        (22345, 6000),
        (33456, 6000),
        (11111, 100),
        (77775, 120),
        (66645, 220),
        (55556, 200),
        (44467, 200),
    ]
)
def test_premio_exactos(num, prize):
    dec = Decimo(num)
    sorteo().check(dec)
    assert dec.prize == prize


@pytest.mark.parametrize(
    "num,prize",
    [
        (12344, 2100),
        (12346, 2100),
        (23455, 1370),
        (23457, 1350),
        (34566, 1060),
        (34568, 1060),
        (67891, 0),
    ]
)
def test_premio_aprox(num, prize):
    dec = Decimo(num)
    sorteo().check(dec)
    assert dec.prize == prize


@pytest.mark.parametrize(
    "num,prize",
    [
        (12333, 100),
        (23476, 100),
        (34599, 100),
        (45622, 100),
        (56745, 220),
        (90198, 0),
    ]
)
def test_premio_centenas(num, prize):
    dec = Decimo(num)
    sorteo().check(dec)
    assert dec.prize == prize


@pytest.mark.parametrize(
    "num,prize",
    [
        (45, 120),
        (56, 100),
        (67, 100),
        (99945, 120),
        (99956, 100),
        (99967, 100),
    ]
)
def test_premio_dosultimas(num, prize):
    dec = Decimo(num)
    sorteo().check(dec)
    assert dec.prize == prize


@pytest.mark.parametrize(
    "num,prize",
    [
        (11115, 20),
        (22225, 20),
    ]
)
def test_premio_reintegro(num, prize):
    dec = Decimo(num)
    sorteo().check(dec)
    assert dec.prize == prize
