from typing import List, Optional
import random


class Decimo:

    def __init__(self, num: Optional[int] = None):
        self.num = num or random.randint(0, 99999)
        self.num_list = Decimo.int_to_list(self.num)
        self.prize = 0

    @staticmethod
    def int_to_list(num: int) -> List[int]:
        return list(map(int, [x for x in str(num)]))

    def equals(self, num: int) -> bool:
        return num == self.num

    def last_n(self, num: int, n: int) -> bool:
        list_num = Decimo.int_to_list(num)
        return list_num[-n:] == self.num_list[-n:]

    def hundreds(self, num: int) -> bool:
        mod = num % 100
        low = num - mod
        high = num + 99 - mod
        return (low <= self.num <= high) and not self.equals(num)

    def aprox(self, num: int) -> bool:
        return num + 1 == self.num or num - 1 == self.num


class Sorteo:

    def __init__(self, nums: Optional[List[int]] = None):
        self.numbers: List[int] = []
        if nums is None:
            for _ in range(1807):
                while (value := random.randint(0, 99999)) in self.numbers:
                    pass
                self.numbers.append(value)
        else:
            self.numbers = nums
        self.prizes = [
            400_000,
            125_000,
            50_000,
            20_000,
            20_000,
            6_000,
            6_000,
            6_000,
            6_000,
            6_000,
            6_000,
            6_000,
            6_000,
        ]
        self.aprox_prizes = [2_000, 1_250, 960]

    def check(self, dec: Decimo) -> None:
        for i, num in enumerate(self.numbers):
            if dec.equals(num):
                if i < 13:
                    dec.prize = self.prizes[i]
                    return
                else:
                    dec.prize = 100
        for i in range(5):
            if i < 3:
                if dec.aprox(self.numbers[i]):
                    dec.prize += self.aprox_prizes[i]
                if dec.last_n(self.numbers[i], 2):
                    dec.prize += 100
            if dec.hundreds(self.numbers[i]):
                dec.prize += 100
        if dec.last_n(self.numbers[0], 1):
            dec.prize += 20
